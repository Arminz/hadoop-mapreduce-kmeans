#!/usr/bin/python

import sys, re, math

CLUSTERS_FILENAME = './k-means/clusters.txt'

clusters = []
delta_clusters = dict()
clusters_points = dict()


def read_from_clusters_cache_file(clusters_file):
    f = open(clusters_file, 'r')
    data = f.read()
    f.close()
    del f
    return data


def read_clusters():
    cluster_data = read_from_clusters_cache_file(CLUSTERS_FILENAME)
    for line in cluster_data.strip().split("\n"):
        # print(line)
        centroid_id, coords = line.strip().split(",")
        centroid_id = int(centroid_id)
        latitude, longitude = coords.strip().split(";")
        clusters.append((centroid_id, float(latitude), float(longitude)))
        # print(clusters)
        delta_clusters[centroid_id] = (0, 0, 0)


def get_distance_coords(lat1, long1, lat2, long2):
    # Calculate euclidian distance between two coordinates
    dist = math.sqrt(math.pow(lat1 - lat2, 2) + math.pow(long1 - long2, 2))
    return dist


def get_nearest_cluster(latitude, longitude):
    nearest_cluster_id = None
    nearest_distance = 1000000000
    for cluster in clusters:
        dist = get_distance_coords(latitude, longitude, cluster[1], cluster[2])
        if dist < nearest_distance:
            nearest_cluster_id = cluster[0]
            nearest_distance = dist
    return nearest_cluster_id


read_clusters()


for line in sys.stdin:
    line = line.strip()
    words = line.split(',')
    # print(words)
    if words is None or len(words) != 2:
        print ("ERROR PARSING LINE (Columns: " + str(len(words)) + ") - ", line)
        continue
    else:
        latitude, longitude = words
        lat = float(latitude)
        long = float(longitude)
        nearest_cluster_id = get_nearest_cluster(lat, long)
        if nearest_cluster_id in clusters_points:
            clusters_points[nearest_cluster_id].append([latitude, longitude])
        else:
            clusters_points[nearest_cluster_id] = []
        # sumy, sumx, cont = delta_clusters[nearest_cluster_id]
        # delta_clusters[nearest_cluster_id] = (sumy + lat, sumx + long, cont + 1)

# for key in delta_clusters:
    # sumy, sumx, cont = delta_clusters[key]
#     print (key + "\t"for + str(sumy) + ";" + str(sumx) + ";" + str(cont))
for cluster_point in clusters_points:
    points = clusters_points[cluster_point]
    points_repr = ""
    for point in points:
        points_repr += "{};{},".format(point[0], point[1])
    points_repr = points_repr[:-1]
    print ('{}\t{}'.format(cluster_point, points_repr))