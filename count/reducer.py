#!/usr/bin/env python
import operator
import sys
import pandas as pd

counts = {}

for line in sys.stdin:

    line = line.strip()
    word, count = line.split("\t")

    count = int(count)

    if word not in counts:
        counts[word] = count
    else:
        counts[word] += count

df = pd.DataFrame.from_dict(counts, orient='index')
# df.sort_values(by=['cnt'], inplace=True, ascending=False)
items = sorted(list(counts.items()),key=lambda x:x[1], reverse=True)
# for word, value in sorted(counts.items(), key=operator.itemgetter(0)):
#     df.append(word, value)

for item in items:
    print("{}\t{}".format(item[0], item[1]))
# print("{}\t{}".format(word, counts[word]))