#!/usr/bin/env python
import sys

counts = {}

for line in sys.stdin:
    line = line.strip()
    words = line.split()
    for word in words:
        if word in counts:
            counts[word] += 1
        else:
            counts[word] = 1
for count in counts:
    print ("{}\t{}".format(count, counts[count]))
